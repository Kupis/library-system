﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LibrarySystem
{
    public partial class addNewForm : Form
    {
        public BookModel book = new BookModel();

        public addNewForm()
        {
            InitializeComponent();

            //DataSource without BookType.allBooks
            addBookTypeComboBox.DataSource = Enum.GetValues(typeof(BookType))
                .Cast<BookType>()
                .Where(p => p != BookType.allBooks)
                .ToArray<BookType>();

            addBookTypeComboBox.SelectedIndex = 0; 
        }

        private void addBookSubmitButton_Click(object sender, EventArgs e)
        {
            BookType type;
            Enum.TryParse<BookType>(addBookTypeComboBox.SelectedValue.ToString(), out type);

            BookModel book = new BookModel();
            book.title = addBookTitleInput.Text;
            book.genre = addBookGenreInput.Text;
            book.publisher = addBookPublisherInput.Text;
            book.description = addBookDescriptionInput.Text;
            book.additional = additionalInputAdd.Text;
            book.type = type;

            book.saveModelToFile();
            this.DialogResult = DialogResult.OK;
            this.book = book;

            this.Close();
        }

        private void addBookTypeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            BookType type;
            Enum.TryParse<BookType>(addBookTypeComboBox.SelectedValue.ToString(), out type);
            if (type == BookType.normalBook)
            {
                labelAdditionalAdd.Text = "Additional";
                labelAdditionalAdd.Visible = false;
                additionalInputAdd.Text = "";
                additionalInputAdd.Visible = false;
            }
            else
            {
                labelAdditionalAdd.Visible = true;
                additionalInputAdd.Visible = true;
                if (type == BookType.whiteCrow)
                {
                    labelAdditionalAdd.Text = "Insert historical";
                }
                else if (type == BookType.schoolReading)
                {
                    labelAdditionalAdd.Text = "Insert year";
                }
            }
        }
    }
}
