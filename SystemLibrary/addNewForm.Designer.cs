﻿namespace LibrarySystem
{
    partial class addNewForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(addNewForm));
            this.tableLayoutAdd = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.addBookTitleInput = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.addBookGenreInput = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.addBookPublisherInput = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.labelAdditionalAdd = new System.Windows.Forms.Label();
            this.additionalInputAdd = new System.Windows.Forms.TextBox();
            this.addBookDescriptionInput = new System.Windows.Forms.RichTextBox();
            this.addBookTypeComboBox = new System.Windows.Forms.ComboBox();
            this.addBookSubmitButton = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.tableLayoutAdd.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutAdd
            // 
            this.tableLayoutAdd.AutoSize = true;
            this.tableLayoutAdd.ColumnCount = 1;
            this.tableLayoutAdd.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutAdd.Controls.Add(this.label1, 0, 0);
            this.tableLayoutAdd.Controls.Add(this.addBookTitleInput, 0, 1);
            this.tableLayoutAdd.Controls.Add(this.label2, 0, 2);
            this.tableLayoutAdd.Controls.Add(this.addBookGenreInput, 0, 3);
            this.tableLayoutAdd.Controls.Add(this.label3, 0, 4);
            this.tableLayoutAdd.Controls.Add(this.addBookPublisherInput, 0, 5);
            this.tableLayoutAdd.Controls.Add(this.label4, 0, 6);
            this.tableLayoutAdd.Controls.Add(this.labelAdditionalAdd, 0, 8);
            this.tableLayoutAdd.Controls.Add(this.additionalInputAdd, 0, 9);
            this.tableLayoutAdd.Controls.Add(this.addBookDescriptionInput, 0, 7);
            this.tableLayoutAdd.Controls.Add(this.addBookTypeComboBox, 0, 11);
            this.tableLayoutAdd.Controls.Add(this.addBookSubmitButton, 0, 12);
            this.tableLayoutAdd.Controls.Add(this.label5, 0, 10);
            this.tableLayoutAdd.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutAdd.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutAdd.Name = "tableLayoutAdd";
            this.tableLayoutAdd.RowCount = 13;
            this.tableLayoutAdd.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutAdd.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutAdd.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutAdd.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutAdd.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutAdd.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutAdd.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutAdd.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutAdd.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutAdd.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutAdd.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutAdd.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutAdd.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutAdd.Size = new System.Drawing.Size(183, 365);
            this.tableLayoutAdd.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(177, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Insert book title";
            this.label1.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // addBookTitleInput
            // 
            this.addBookTitleInput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.addBookTitleInput.Location = new System.Drawing.Point(3, 19);
            this.addBookTitleInput.Name = "addBookTitleInput";
            this.addBookTitleInput.Size = new System.Drawing.Size(177, 20);
            this.addBookTitleInput.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(3, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(177, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "Insert book genre ";
            this.label2.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // addBookGenreInput
            // 
            this.addBookGenreInput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.addBookGenreInput.Location = new System.Drawing.Point(3, 61);
            this.addBookGenreInput.Name = "addBookGenreInput";
            this.addBookGenreInput.Size = new System.Drawing.Size(177, 20);
            this.addBookGenreInput.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(3, 84);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(177, 16);
            this.label3.TabIndex = 4;
            this.label3.Text = "Insert publisher";
            this.label3.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // addBookPublisherInput
            // 
            this.addBookPublisherInput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.addBookPublisherInput.Location = new System.Drawing.Point(3, 103);
            this.addBookPublisherInput.Name = "addBookPublisherInput";
            this.addBookPublisherInput.Size = new System.Drawing.Size(177, 20);
            this.addBookPublisherInput.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.Location = new System.Drawing.Point(3, 126);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(177, 16);
            this.label4.TabIndex = 6;
            this.label4.Text = "Insert book description";
            this.label4.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // labelAdditionalAdd
            // 
            this.labelAdditionalAdd.AutoSize = true;
            this.labelAdditionalAdd.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelAdditionalAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelAdditionalAdd.Location = new System.Drawing.Point(3, 244);
            this.labelAdditionalAdd.Name = "labelAdditionalAdd";
            this.labelAdditionalAdd.Size = new System.Drawing.Size(177, 16);
            this.labelAdditionalAdd.TabIndex = 9;
            this.labelAdditionalAdd.Text = "Additional Insert";
            this.labelAdditionalAdd.Visible = false;
            // 
            // additionalInputAdd
            // 
            this.additionalInputAdd.Dock = System.Windows.Forms.DockStyle.Fill;
            this.additionalInputAdd.Location = new System.Drawing.Point(3, 263);
            this.additionalInputAdd.Name = "additionalInputAdd";
            this.additionalInputAdd.Size = new System.Drawing.Size(177, 20);
            this.additionalInputAdd.TabIndex = 10;
            this.additionalInputAdd.Visible = false;
            // 
            // addBookDescriptionInput
            // 
            this.addBookDescriptionInput.Dock = System.Windows.Forms.DockStyle.Fill;
            this.addBookDescriptionInput.Location = new System.Drawing.Point(3, 145);
            this.addBookDescriptionInput.Name = "addBookDescriptionInput";
            this.addBookDescriptionInput.Size = new System.Drawing.Size(177, 96);
            this.addBookDescriptionInput.TabIndex = 7;
            this.addBookDescriptionInput.Text = "";
            // 
            // addBookTypeComboBox
            // 
            this.addBookTypeComboBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.addBookTypeComboBox.FormattingEnabled = true;
            this.addBookTypeComboBox.Location = new System.Drawing.Point(3, 305);
            this.addBookTypeComboBox.Name = "addBookTypeComboBox";
            this.addBookTypeComboBox.Size = new System.Drawing.Size(177, 21);
            this.addBookTypeComboBox.TabIndex = 12;
            this.addBookTypeComboBox.SelectedIndexChanged += new System.EventHandler(this.addBookTypeComboBox_SelectedIndexChanged);
            // 
            // addBookSubmitButton
            // 
            this.addBookSubmitButton.AutoSize = true;
            this.addBookSubmitButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.addBookSubmitButton.Location = new System.Drawing.Point(3, 332);
            this.addBookSubmitButton.MaximumSize = new System.Drawing.Size(0, 30);
            this.addBookSubmitButton.Name = "addBookSubmitButton";
            this.addBookSubmitButton.Size = new System.Drawing.Size(177, 30);
            this.addBookSubmitButton.TabIndex = 8;
            this.addBookSubmitButton.Text = "Add new book";
            this.addBookSubmitButton.UseVisualStyleBackColor = true;
            this.addBookSubmitButton.Click += new System.EventHandler(this.addBookSubmitButton_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label5.Location = new System.Drawing.Point(3, 286);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(177, 16);
            this.label5.TabIndex = 11;
            this.label5.Text = "Select book type";
            // 
            // addNewForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(183, 365);
            this.Controls.Add(this.tableLayoutAdd);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "addNewForm";
            this.Text = "Add new book";
            this.tableLayoutAdd.ResumeLayout(false);
            this.tableLayoutAdd.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutAdd;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox addBookTitleInput;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox addBookGenreInput;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox addBookPublisherInput;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label labelAdditionalAdd;
        private System.Windows.Forms.TextBox additionalInputAdd;
        private System.Windows.Forms.RichTextBox addBookDescriptionInput;
        private System.Windows.Forms.Button addBookSubmitButton;
        private System.Windows.Forms.ComboBox addBookTypeComboBox;
        private System.Windows.Forms.Label label5;
    }
}