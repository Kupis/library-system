﻿namespace LibrarySystem
{
    partial class mainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(mainForm));
            this.addButton = new System.Windows.Forms.Button();
            this.editButton = new System.Windows.Forms.Button();
            this.deleteButton = new System.Windows.Forms.Button();
            this.searchButton = new System.Windows.Forms.Button();
            this.mainListView = new System.Windows.Forms.ListView();
            this.bookTypeComboBox = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.searchTitleTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.searchGenreTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.searchPublisherTextBox = new System.Windows.Forms.TextBox();
            this.searchDescriptionTextBox = new System.Windows.Forms.TextBox();
            this.searchAdditionalLabel = new System.Windows.Forms.Label();
            this.searchAdditionalTextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // addButton
            // 
            this.addButton.Location = new System.Drawing.Point(600, 12);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(221, 23);
            this.addButton.TabIndex = 1;
            this.addButton.Text = "Add new book";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // editButton
            // 
            this.editButton.Location = new System.Drawing.Point(600, 41);
            this.editButton.Name = "editButton";
            this.editButton.Size = new System.Drawing.Size(221, 23);
            this.editButton.TabIndex = 2;
            this.editButton.Text = "Edit book";
            this.editButton.UseVisualStyleBackColor = true;
            this.editButton.Click += new System.EventHandler(this.editButton_Click);
            // 
            // deleteButton
            // 
            this.deleteButton.Location = new System.Drawing.Point(600, 70);
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Size = new System.Drawing.Size(221, 23);
            this.deleteButton.TabIndex = 3;
            this.deleteButton.Text = "Delete book";
            this.deleteButton.UseVisualStyleBackColor = true;
            this.deleteButton.Click += new System.EventHandler(this.deleteButton_Click);
            // 
            // searchButton
            // 
            this.searchButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.searchButton.Location = new System.Drawing.Point(3, 198);
            this.searchButton.Name = "searchButton";
            this.searchButton.Size = new System.Drawing.Size(209, 88);
            this.searchButton.TabIndex = 4;
            this.searchButton.Text = "Search";
            this.searchButton.UseVisualStyleBackColor = true;
            this.searchButton.Click += new System.EventHandler(this.searchButton_Click);
            // 
            // mainListView
            // 
            this.mainListView.FullRowSelect = true;
            this.mainListView.GridLines = true;
            this.mainListView.HideSelection = false;
            this.mainListView.Location = new System.Drawing.Point(12, 39);
            this.mainListView.MultiSelect = false;
            this.mainListView.Name = "mainListView";
            this.mainListView.Size = new System.Drawing.Size(582, 368);
            this.mainListView.TabIndex = 5;
            this.mainListView.UseCompatibleStateImageBehavior = false;
            this.mainListView.View = System.Windows.Forms.View.Details;
            this.mainListView.DoubleClick += new System.EventHandler(this.mainListView_DoubleClick);
            // 
            // bookTypeComboBox
            // 
            this.bookTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.bookTypeComboBox.FormattingEnabled = true;
            this.bookTypeComboBox.Items.AddRange(new object[] {
            "All books",
            "Normal books",
            "White crow books",
            "School reading books"});
            this.bookTypeComboBox.Location = new System.Drawing.Point(12, 12);
            this.bookTypeComboBox.Name = "bookTypeComboBox";
            this.bookTypeComboBox.Size = new System.Drawing.Size(582, 21);
            this.bookTypeComboBox.TabIndex = 6;
            this.bookTypeComboBox.SelectedIndexChanged += new System.EventHandler(this.bookTypeComboBox_SelectedIndexChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tableLayoutPanel1);
            this.groupBox1.Location = new System.Drawing.Point(600, 99);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(221, 308);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Search panel";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.searchButton, 0, 10);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.searchTitleTextBox, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.searchGenreTextBox, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.searchPublisherTextBox, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.searchDescriptionTextBox, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.searchAdditionalLabel, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.searchAdditionalTextBox, 0, 9);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 6);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 11;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(215, 289);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(209, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Title";
            // 
            // searchTitleTextBox
            // 
            this.searchTitleTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.searchTitleTextBox.Location = new System.Drawing.Point(3, 16);
            this.searchTitleTextBox.Name = "searchTitleTextBox";
            this.searchTitleTextBox.Size = new System.Drawing.Size(209, 20);
            this.searchTitleTextBox.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Location = new System.Drawing.Point(3, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(209, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Genre";
            // 
            // searchGenreTextBox
            // 
            this.searchGenreTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.searchGenreTextBox.Location = new System.Drawing.Point(3, 55);
            this.searchGenreTextBox.Name = "searchGenreTextBox";
            this.searchGenreTextBox.Size = new System.Drawing.Size(209, 20);
            this.searchGenreTextBox.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Location = new System.Drawing.Point(3, 78);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(209, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Publisher";
            // 
            // searchPublisherTextBox
            // 
            this.searchPublisherTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.searchPublisherTextBox.Location = new System.Drawing.Point(3, 94);
            this.searchPublisherTextBox.Name = "searchPublisherTextBox";
            this.searchPublisherTextBox.Size = new System.Drawing.Size(209, 20);
            this.searchPublisherTextBox.TabIndex = 10;
            // 
            // searchDescriptionTextBox
            // 
            this.searchDescriptionTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.searchDescriptionTextBox.Location = new System.Drawing.Point(3, 133);
            this.searchDescriptionTextBox.Name = "searchDescriptionTextBox";
            this.searchDescriptionTextBox.Size = new System.Drawing.Size(209, 20);
            this.searchDescriptionTextBox.TabIndex = 12;
            // 
            // searchAdditionalLabel
            // 
            this.searchAdditionalLabel.AutoSize = true;
            this.searchAdditionalLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.searchAdditionalLabel.Location = new System.Drawing.Point(3, 156);
            this.searchAdditionalLabel.Name = "searchAdditionalLabel";
            this.searchAdditionalLabel.Size = new System.Drawing.Size(209, 13);
            this.searchAdditionalLabel.TabIndex = 13;
            this.searchAdditionalLabel.Text = "Additional";
            this.searchAdditionalLabel.Visible = false;
            // 
            // searchAdditionalTextBox
            // 
            this.searchAdditionalTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.searchAdditionalTextBox.Location = new System.Drawing.Point(3, 172);
            this.searchAdditionalTextBox.Name = "searchAdditionalTextBox";
            this.searchAdditionalTextBox.Size = new System.Drawing.Size(209, 20);
            this.searchAdditionalTextBox.TabIndex = 14;
            this.searchAdditionalTextBox.Visible = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Location = new System.Drawing.Point(3, 117);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(209, 13);
            this.label5.TabIndex = 15;
            this.label5.Text = "Description";
            // 
            // mainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(834, 422);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.bookTypeComboBox);
            this.Controls.Add(this.mainListView);
            this.Controls.Add(this.deleteButton);
            this.Controls.Add(this.editButton);
            this.Controls.Add(this.addButton);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(850, 460);
            this.MinimumSize = new System.Drawing.Size(850, 460);
            this.Name = "mainForm";
            this.Text = "Library System";
            this.Load += new System.EventHandler(this.mainForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.Button editButton;
        private System.Windows.Forms.Button deleteButton;
        private System.Windows.Forms.Button searchButton;
        private System.Windows.Forms.ListView mainListView;
        private System.Windows.Forms.ComboBox bookTypeComboBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox searchTitleTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox searchGenreTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox searchPublisherTextBox;
        private System.Windows.Forms.TextBox searchDescriptionTextBox;
        private System.Windows.Forms.Label searchAdditionalLabel;
        private System.Windows.Forms.TextBox searchAdditionalTextBox;
        private System.Windows.Forms.Label label5;
    }
}

