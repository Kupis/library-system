﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LibrarySystem
{
    public enum BookType { allBooks = 0, normalBook = 1, whiteCrow = 2, schoolReading = 3 }

    public partial class mainForm : Form
    {
        static List<BookModel> booksList = new List<BookModel>();
        private BookType mainListViewOption;

        public mainForm()
        {
            InitializeComponent();
        }

        private void mainForm_Load(object sender, EventArgs e)
        {
            BookModel book = new BookModel();
            book.loadAllModelsFromFile(booksList);

            bookTypeComboBox.DataSource = Enum.GetValues(typeof(BookType));

            bookTypeComboBox.SelectedItem = BookType.allBooks ;
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            using (var addForm = new addNewForm())
            {
                addForm.ShowDialog();
                if (addForm.DialogResult == DialogResult.OK)
                {
                    booksList.Add(addForm.book);
                    reloadMainListView();
                }
            }
        }

        private void searchButton_Click(object sender, EventArgs e)
        {
            resetMainListView();

            bool sourceContainsSearchedValue;
            foreach (var book in booksList)
            {
                sourceContainsSearchedValue = true;

                if (mainListViewOption != BookType.allBooks)
                {
                    if ((int) book.type != (int) mainListViewOption)
                    {
                        sourceContainsSearchedValue = false;
                        continue;
                    }
                }
                if (!book.title.Contains(searchTitleTextBox.Text))
                {
                    sourceContainsSearchedValue = false;
                    continue;
                }
                if (!book.genre.Contains(searchGenreTextBox.Text))
                {
                    sourceContainsSearchedValue = false;
                    continue;
                }
                if (!book.publisher.Contains(searchPublisherTextBox.Text))
                {
                    sourceContainsSearchedValue = false;
                    continue;
                }
                if (!book.description.Contains(searchDescriptionTextBox.Text))
                {
                    sourceContainsSearchedValue = false;
                    continue;
                }
                if (searchAdditionalTextBox.Visible == true)
                {
                    if (!book.additional.Contains(searchAdditionalTextBox.Text))
                    {
                        sourceContainsSearchedValue = false;
                        continue;
                    }
                }

                if (sourceContainsSearchedValue)
                {
                    addBookToMainListView(book);
                }
            }
        }

        private void addBookToMainListView (BookModel book)
        {
            mainListView.Items.Add(new ListViewItem(new string[] { book.guid.ToString(), book.title, book.genre, book.publisher, book.description, book.additional }));

            if (book.type == BookType.normalBook)
            {
                mainListView.Items[mainListView.Items.Count - 1].Group = mainListView.Groups[0];
            }
            else if (book.type == BookType.whiteCrow)
            {
                mainListView.Items[mainListView.Items.Count - 1].Group = mainListView.Groups[1];
            }
            else if (book.type ==  BookType.schoolReading)
            {
                mainListView.Items[mainListView.Items.Count - 1].Group = mainListView.Groups[2];
            }
            else
            {
                MessageBox.Show("Failed adding book to list view");
            }
        }

        public void resetMainListView()
        {
            mainListView.Clear();

            mainListView.Columns.Add("GUID");
            mainListView.Columns[0].Width = 0;
            mainListView.Columns.Add("Title");
            mainListView.Columns.Add("Literary genre");
            mainListView.Columns.Add("Publisher");
            mainListView.Columns.Add("Description");

            if (mainListViewOption == BookType.whiteCrow)
            {
                mainListView.Columns.Add("History"); ;
            }
            else if (mainListViewOption == BookType.schoolReading)
            {
                mainListView.Columns.Add("Year");
            }
            else if (mainListViewOption == BookType.allBooks)
            {
                mainListView.Columns.Add("Additional");

                mainListView.Groups.Add(new ListViewGroup("Normal books"));
                mainListView.Groups.Add(new ListViewGroup("White crow books"));
                mainListView.Groups.Add(new ListViewGroup("School reading books"));
            }
        }

        public void reloadMainListView()
        {
            resetMainListView();

            if (mainListViewOption == BookType.allBooks)
            {
                foreach (var book in booksList)
                {
                    mainListView.Items.Add(new ListViewItem(new string[] { book.guid.ToString(), book.title, book.genre, book.publisher, book.description, book.additional }));
                    groupBook(book);
                }
            }
            else
            {
                foreach (var book in booksList)
                {
                    if ((int) book.type == (int) mainListViewOption)
                    {
                        mainListView.Items.Add(new ListViewItem(new string[] { book.guid.ToString(), book.title, book.genre, book.publisher, book.description, book.additional }));
                    }
                }
            }
        }

        private void groupBook(BookModel book)
        {
            if (book.type == BookType.normalBook)
            {
                mainListView.Items[mainListView.Items.Count - 1].Group = mainListView.Groups[0];
            }
            else if (book.type == BookType.whiteCrow)
            {
                mainListView.Items[mainListView.Items.Count - 1].Group = mainListView.Groups[1];
            }
            else if (book.type == BookType.schoolReading)
            {
                mainListView.Items[mainListView.Items.Count - 1].Group = mainListView.Groups[2];
            }
            else
            {
                MessageBox.Show("Failed adding book to group for list view");
            }
        }

        private int getListIndexForGUID(Guid guid)
        {
            int index = -1;
            foreach (var book in booksList)
            {
                index++;

                if (book.guid == guid)
                {
                    return index;
                }
            }
            return -1; //Index not found
        }

        private void editButton_Click(object sender, EventArgs e)
        {
            Guid guid = new Guid(mainListView.SelectedItems[0].Text);

            int index = getListIndexForGUID(guid);
            if (index == -1)
            {
                MessageBox.Show("Index for editButton_Click not found");
            }
            else
            {
                checkTypeAndEditBook(index);
            }
       }

        private void checkTypeAndEditBook(int index)
        {
            BookModel book = new BookModel();
            book.title = booksList[index].title;
            book.genre = booksList[index].genre;
            book.publisher = booksList[index].publisher;
            book.description = booksList[index].description;
            book.type = booksList[index].type;
            book.additional = booksList[index].additional;

            using (var editForm = new editForm(book))
            {
                editForm.ShowDialog();
                if (editForm.DialogResult == DialogResult.OK)
                {
                    booksList[index] = editForm.book;
                    book.editBookFromList(booksList);
                    reloadMainListView();
                }
            }
        }

        private void hideSearchAdditionalTextBox()
        {
            searchAdditionalLabel.Visible = false;
            searchAdditionalTextBox.Text = "";
            searchAdditionalTextBox.Visible = false;
        }

        private void showSearchAdditionalTextBox()
        {
            searchAdditionalLabel.Visible = true;
            searchAdditionalLabel.Name = "Additional";
            searchAdditionalTextBox.Visible = true;
        }

        private void bookTypeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            Enum.TryParse<BookType>(bookTypeComboBox.SelectedValue.ToString(), out mainListViewOption);

            if (mainListViewOption == BookType.normalBook)
            {
                hideSearchAdditionalTextBox();
            }
            else
            {
                showSearchAdditionalTextBox();
            }

            reloadMainListView();
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            Guid guid = new Guid(mainListView.SelectedItems[0].Text);

            int index = getListIndexForGUID(guid);
            if (index == -1)
            {
                MessageBox.Show("Index for deleteButton_Click not found");
            }
            else
            {
                BookModel book = new BookModel();
                booksList.RemoveAt(index);
                book.editBookFromList(booksList);
                reloadMainListView();     
            }
        }

        private void mainListView_DoubleClick(object sender, EventArgs e)
        {
            Guid guid = new Guid(mainListView.SelectedItems[0].Text);

            int index = getListIndexForGUID(guid);
            if (index == -1)
            {
                MessageBox.Show("Index for mainListView-DoubleClick not found");
            }
            else
            {
                BookModel book = new BookModel();
                book.title = booksList[index].title;
                book.genre = booksList[index].genre;
                book.publisher = booksList[index].publisher;
                book.description = booksList[index].description;
                book.type = booksList[index].type;
                book.additional = booksList[index].additional;

                using (var viewForm = new viewForm(book))
                {
                    viewForm.ShowDialog();
                }
            }
        }
    }
}
