﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json;
using System.Windows.Forms;

namespace LibrarySystem
{
    public class BookModel
    {
        static string path = @"Books.txt";
        static protected string temporaryPath = @"TemporaryBooks.txt";
        static protected string backupPath = null;

        public string title { get; set; }
        public string genre { get; set; }
        public string publisher { get; set; }
        public string description { get; set; }
        public string additional { get; set; }
        public BookType type { get; set; }
        public Guid guid;

        public BookModel()
        {
            guid = Guid.NewGuid();
        }

        public void saveModelToFile()
        {
            if(!File.Exists(path))
            {
                File.CreateText(path).Close();
            }

            string output = JsonConvert.SerializeObject(this);

            StreamWriter sw = File.AppendText(path);
            sw.WriteLine(output);
            sw.Close();
        }

        public List<BookModel> loadAllModelsFromFile(List<BookModel> books)
        {
            if (File.Exists(path))
            {
                var jsons = File.ReadLines(path);
                foreach (var json in jsons)
                {
                    BookModel book = JsonConvert.DeserializeObject<BookModel>(json);
                    books.Add(book);
                }
            }
            return books;
        }

        public void editBookFromList(List<BookModel> books)
        {
            try
            {
                File.CreateText(temporaryPath).Close();
            }
            catch
            {
                File.Delete(temporaryPath);
            }
            finally
            {
                File.CreateText(temporaryPath).Close();
            }

            using (StreamWriter sw = File.AppendText(temporaryPath))
            {
                foreach (var book in books)
                {
                    string output = JsonConvert.SerializeObject(book);
                    sw.WriteLine(output);
                }
                sw.Close();
            }
            try
            {
                File.Replace(temporaryPath, path, backupPath);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }
    }
}
