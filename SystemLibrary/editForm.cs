﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LibrarySystem
{
    public partial class editForm : Form
    {
        public BookModel book = new BookModel();

        public editForm(BookModel book)
        {
            InitializeComponent();

            //DataSource without BookType.allBooks
            editBookTypeComboBox.DataSource = Enum.GetValues(typeof(BookType))
                .Cast<BookType>()
                .Where(p => p != BookType.allBooks)
                .ToArray<BookType>();

            this.editTitle.Text = book.title;
            this.editGenre.Text = book.genre;
            this.editPublisher.Text = book.publisher;
            this.editDescription.Text = book.description;
            this.additionalTextBoxEdit.Text = book.additional;
            this.editBookTypeComboBox.SelectedItem = book.type;
        }

        private void editSubmit_Click(object sender, EventArgs e)
        {
            BookType type;
            Enum.TryParse<BookType>(editBookTypeComboBox.SelectedValue.ToString(), out type);

            BookModel book = new BookModel();
            book.title = editTitle.Text;
            book.genre = editGenre.Text;
            book.publisher = editPublisher.Text;
            book.description = editDescription.Text;
            book.additional = additionalTextBoxEdit.Text;
            book.type = type;

            book.saveModelToFile();

            this.DialogResult = DialogResult.OK;
            this.book = book;
            this.Close();
        }

        private void editBookTypeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            BookType type;
            Enum.TryParse<BookType>(editBookTypeComboBox.SelectedValue.ToString(), out type);

            if (type == BookType.normalBook)
            {
                additionalLabelEdit.Text = "Additional";
                additionalLabelEdit.Visible = false;
                additionalTextBoxEdit.Text = "";
                additionalTextBoxEdit.Visible = false;
            }
            else
            {
                additionalLabelEdit.Visible = true;
                additionalTextBoxEdit.Visible = true;
                if (type == BookType.whiteCrow)
                {
                    additionalLabelEdit.Text = "Edit historical";
                }
                else if (type == BookType.schoolReading)
                {
                    additionalLabelEdit.Text = "Edit year";
                }
            }
        }
    }
}
