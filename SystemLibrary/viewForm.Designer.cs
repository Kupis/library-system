﻿namespace LibrarySystem
{
    partial class viewForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(viewForm));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.viewTitleTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.viewGenreTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.viewPublisherTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.viewDescriptionRichtextBox = new System.Windows.Forms.RichTextBox();
            this.viewAdditionalLabel = new System.Windows.Forms.Label();
            this.viewAdditionalTextBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.viewTypeTextBox = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.viewTitleTextBox, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.viewGenreTextBox, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.viewPublisherTextBox, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.viewDescriptionRichtextBox, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.viewAdditionalLabel, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.viewAdditionalTextBox, 0, 9);
            this.tableLayoutPanel1.Controls.Add(this.label6, 0, 10);
            this.tableLayoutPanel1.Controls.Add(this.viewTypeTextBox, 0, 11);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 12;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(252, 296);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(246, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Title";
            // 
            // viewTitleTextBox
            // 
            this.viewTitleTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.viewTitleTextBox.Location = new System.Drawing.Point(3, 19);
            this.viewTitleTextBox.Name = "viewTitleTextBox";
            this.viewTitleTextBox.ReadOnly = true;
            this.viewTitleTextBox.Size = new System.Drawing.Size(246, 20);
            this.viewTitleTextBox.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(3, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(246, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "Genre";
            // 
            // viewGenreTextBox
            // 
            this.viewGenreTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.viewGenreTextBox.Location = new System.Drawing.Point(3, 61);
            this.viewGenreTextBox.Name = "viewGenreTextBox";
            this.viewGenreTextBox.ReadOnly = true;
            this.viewGenreTextBox.Size = new System.Drawing.Size(246, 20);
            this.viewGenreTextBox.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(3, 84);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(246, 16);
            this.label3.TabIndex = 4;
            this.label3.Text = "Publisher";
            // 
            // viewPublisherTextBox
            // 
            this.viewPublisherTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.viewPublisherTextBox.Location = new System.Drawing.Point(3, 103);
            this.viewPublisherTextBox.Name = "viewPublisherTextBox";
            this.viewPublisherTextBox.ReadOnly = true;
            this.viewPublisherTextBox.Size = new System.Drawing.Size(246, 20);
            this.viewPublisherTextBox.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.Location = new System.Drawing.Point(3, 126);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(246, 16);
            this.label4.TabIndex = 6;
            this.label4.Text = "Description";
            // 
            // viewDescriptionRichtextBox
            // 
            this.viewDescriptionRichtextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.viewDescriptionRichtextBox.Location = new System.Drawing.Point(3, 145);
            this.viewDescriptionRichtextBox.Name = "viewDescriptionRichtextBox";
            this.viewDescriptionRichtextBox.ReadOnly = true;
            this.viewDescriptionRichtextBox.Size = new System.Drawing.Size(246, 62);
            this.viewDescriptionRichtextBox.TabIndex = 11;
            this.viewDescriptionRichtextBox.Text = "";
            // 
            // viewAdditionalLabel
            // 
            this.viewAdditionalLabel.AutoSize = true;
            this.viewAdditionalLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.viewAdditionalLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.viewAdditionalLabel.Location = new System.Drawing.Point(3, 210);
            this.viewAdditionalLabel.Name = "viewAdditionalLabel";
            this.viewAdditionalLabel.Size = new System.Drawing.Size(246, 16);
            this.viewAdditionalLabel.TabIndex = 12;
            this.viewAdditionalLabel.Text = "Additional";
            // 
            // viewAdditionalTextBox
            // 
            this.viewAdditionalTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.viewAdditionalTextBox.Location = new System.Drawing.Point(3, 229);
            this.viewAdditionalTextBox.Name = "viewAdditionalTextBox";
            this.viewAdditionalTextBox.ReadOnly = true;
            this.viewAdditionalTextBox.Size = new System.Drawing.Size(246, 20);
            this.viewAdditionalTextBox.TabIndex = 13;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label6.Location = new System.Drawing.Point(3, 252);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(246, 16);
            this.label6.TabIndex = 14;
            this.label6.Text = "Type";
            // 
            // viewTypeTextBox
            // 
            this.viewTypeTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.viewTypeTextBox.Location = new System.Drawing.Point(3, 271);
            this.viewTypeTextBox.Name = "viewTypeTextBox";
            this.viewTypeTextBox.ReadOnly = true;
            this.viewTypeTextBox.Size = new System.Drawing.Size(246, 20);
            this.viewTypeTextBox.TabIndex = 15;
            // 
            // viewForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(252, 296);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "viewForm";
            this.Text = "View book";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox viewTitleTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox viewGenreTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox viewPublisherTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.RichTextBox viewDescriptionRichtextBox;
        private System.Windows.Forms.Label viewAdditionalLabel;
        private System.Windows.Forms.TextBox viewAdditionalTextBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox viewTypeTextBox;
    }
}