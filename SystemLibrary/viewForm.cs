﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LibrarySystem
{
    public partial class viewForm : Form
    {
        public viewForm(BookModel book)
        {
            InitializeComponent();

            if (book.type == BookType.normalBook)
            {
                this.viewAdditionalTextBox.Visible = false;
                this.viewAdditionalLabel.Visible = false;
            }

            this.viewTitleTextBox.Text = book.title;
            this.viewGenreTextBox.Text = book.genre;
            this.viewPublisherTextBox.Text = book.publisher;
            this.viewDescriptionRichtextBox.Text = book.description;
            this.viewAdditionalTextBox.Text = book.additional;
            this.viewTypeTextBox.Text = book.type.ToString();
        }
    }
}
