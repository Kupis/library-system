﻿namespace LibrarySystem
{
    partial class editForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(editForm));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.editTitle = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.editGenre = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.editPublisher = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.additionalLabelEdit = new System.Windows.Forms.Label();
            this.additionalTextBoxEdit = new System.Windows.Forms.TextBox();
            this.editDescription = new System.Windows.Forms.RichTextBox();
            this.editBookTypeComboBox = new System.Windows.Forms.ComboBox();
            this.editSubmit = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.AutoSize = true;
            this.tableLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.editTitle, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.editGenre, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.editPublisher, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.additionalLabelEdit, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.additionalTextBoxEdit, 0, 9);
            this.tableLayoutPanel1.Controls.Add(this.editDescription, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.editBookTypeComboBox, 0, 11);
            this.tableLayoutPanel1.Controls.Add(this.editSubmit, 0, 12);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 10);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 13;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(177, 365);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(171, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Edit book title";
            this.label1.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // editTitle
            // 
            this.editTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.editTitle.Location = new System.Drawing.Point(3, 19);
            this.editTitle.Name = "editTitle";
            this.editTitle.Size = new System.Drawing.Size(171, 20);
            this.editTitle.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(3, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(171, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "Edit book genre";
            this.label2.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // editGenre
            // 
            this.editGenre.Dock = System.Windows.Forms.DockStyle.Fill;
            this.editGenre.Location = new System.Drawing.Point(3, 61);
            this.editGenre.Name = "editGenre";
            this.editGenre.Size = new System.Drawing.Size(171, 20);
            this.editGenre.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(3, 84);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(171, 16);
            this.label3.TabIndex = 4;
            this.label3.Text = "Edit book publisher";
            this.label3.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // editPublisher
            // 
            this.editPublisher.Dock = System.Windows.Forms.DockStyle.Fill;
            this.editPublisher.Location = new System.Drawing.Point(3, 103);
            this.editPublisher.Name = "editPublisher";
            this.editPublisher.Size = new System.Drawing.Size(171, 20);
            this.editPublisher.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.Location = new System.Drawing.Point(3, 126);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(171, 16);
            this.label4.TabIndex = 6;
            this.label4.Text = "Edit book description";
            this.label4.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // additionalLabelEdit
            // 
            this.additionalLabelEdit.AutoSize = true;
            this.additionalLabelEdit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.additionalLabelEdit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.additionalLabelEdit.Location = new System.Drawing.Point(3, 244);
            this.additionalLabelEdit.Name = "additionalLabelEdit";
            this.additionalLabelEdit.Size = new System.Drawing.Size(171, 16);
            this.additionalLabelEdit.TabIndex = 9;
            this.additionalLabelEdit.Text = "Additional insert";
            this.additionalLabelEdit.Visible = false;
            // 
            // additionalTextBoxEdit
            // 
            this.additionalTextBoxEdit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.additionalTextBoxEdit.Location = new System.Drawing.Point(3, 263);
            this.additionalTextBoxEdit.Name = "additionalTextBoxEdit";
            this.additionalTextBoxEdit.Size = new System.Drawing.Size(171, 20);
            this.additionalTextBoxEdit.TabIndex = 10;
            this.additionalTextBoxEdit.Visible = false;
            // 
            // editDescription
            // 
            this.editDescription.Dock = System.Windows.Forms.DockStyle.Fill;
            this.editDescription.Location = new System.Drawing.Point(3, 145);
            this.editDescription.Name = "editDescription";
            this.editDescription.Size = new System.Drawing.Size(171, 96);
            this.editDescription.TabIndex = 11;
            this.editDescription.Text = "";
            // 
            // editBookTypeComboBox
            // 
            this.editBookTypeComboBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.editBookTypeComboBox.FormattingEnabled = true;
            this.editBookTypeComboBox.Location = new System.Drawing.Point(3, 305);
            this.editBookTypeComboBox.Name = "editBookTypeComboBox";
            this.editBookTypeComboBox.Size = new System.Drawing.Size(171, 21);
            this.editBookTypeComboBox.TabIndex = 13;
            this.editBookTypeComboBox.SelectedIndexChanged += new System.EventHandler(this.editBookTypeComboBox_SelectedIndexChanged);
            // 
            // editSubmit
            // 
            this.editSubmit.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.editSubmit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.editSubmit.Location = new System.Drawing.Point(3, 332);
            this.editSubmit.MaximumSize = new System.Drawing.Size(0, 30);
            this.editSubmit.Name = "editSubmit";
            this.editSubmit.Size = new System.Drawing.Size(171, 30);
            this.editSubmit.TabIndex = 8;
            this.editSubmit.Text = "Edit";
            this.editSubmit.UseVisualStyleBackColor = true;
            this.editSubmit.Click += new System.EventHandler(this.editSubmit_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label5.Location = new System.Drawing.Point(3, 286);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(171, 16);
            this.label5.TabIndex = 12;
            this.label5.Text = "Edit book type";
            // 
            // editForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(177, 365);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "editForm";
            this.Text = "Edit book";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox editTitle;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox editGenre;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox editPublisher;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button editSubmit;
        private System.Windows.Forms.Label additionalLabelEdit;
        private System.Windows.Forms.TextBox additionalTextBoxEdit;
        private System.Windows.Forms.RichTextBox editDescription;
        private System.Windows.Forms.ComboBox editBookTypeComboBox;
        private System.Windows.Forms.Label label5;
    }
}