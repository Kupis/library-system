# Library System
.Net application to store, view, add, edit and delete books.
Created to learn more about .net and windows forms.
Books have 3 categories: normal books, white crows and school books.
## Author
* **Patryk Kupis** - [Patryk Kupis](https://gitlab.com/Kupis)
## License
This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.
